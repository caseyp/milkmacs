;;; init.el --- Emacs Init File -*- lexical-binding: t; -*-
;;;
;;; Commentary:
;;;   Emilks primary initialization file.
;;;
;;; Code:

;; TODO Determine proper keybinds
;; TODO https://github.com/karthink/consult-dir

(add-to-list 'load-path (locate-user-emacs-file "local/"))
(require 'milk-lib)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; Configuration Group

(require 'milk-config)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; Initialize Straight Repository
;;;
;;; Code:

(setq-default straight-base-dir                (locate-expanded-emacs-file "var/")
              straight-check-for-modifications '(check-on-save find-when-checking)
              straight-use-package-by-default   t
              straight-fix-flycheck             t)

(defvar bootstrap-version)
(let ((bootstrap-file
       (locate-expanded-emacs-file "var/straight/repos/straight.el/bootstrap.el"))
      (bootstrap-version 6))
  (load bootstrap-file nil 'nomessage))

(require 'straight)
(straight-use-package 'use-package)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; Package Management
;;;
;;; Code:

(straight-register-package 'org)
(straight-register-package 'org-contrib)
(straight-register-package 'eglot)

(use-feature use-package)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;; Add extra information on completion.
;; Also used for defining completion categories.
(use-package marginalia
  :config
  (marginalia-mode))


;; In middle of key chord, hit C-h to view options for continuation.
;;
;; NOTE: Will be replaced by embark-prefix-help-command in the future, but we
;; need which-key to support a better embark-indicator for now.
(use-package which-key
  :custom
  (which-key-sort-order           'which-key-local-then-key-order)
  (which-key-show-early-on-C-h    t)
  (which-key-idle-delay           7)
  (which-key-idle-secondary-delay 1e-100)
  :config
  (which-key-mode))


(require 'milk-embark)

(use-package consult
  :after (embark-consult)
  :demand t)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; Emacs System
;;;
;;; Configuration:

(use-package vlf
  :bind (:map embark-file-map
              ("l" . vlf)))

(use-package sudo-edit
  :bind (:map embark-file-map
              ("s" . sudo-edit)))

;; TODO Move
;; (defvar filter-messages '())

;; (defun filter-messages--around (fun &rest rest)
;;   ""
;;   (unless (seq-some (lambda (elt) (string-match elt (or (car rest) ""))) filter-messages)
;;     (apply fun rest)))

;; (advice-add 'message :around #'filter-messages--around)

(use-feature emacs
  :custom
  (custom-file nil)
  :config
  ;; TODO rebind edebug X
  ;; TODO next error
  ;; TODO abbrev (a)
  ;; TODO kmacro
  ;; TODO move mark whole buffer (h)
  ;; TODO project prefix (p)
  ;; TODO move text-scale adjust
  ;; TODO move comments line (C-;)
  ;; TODO explore macros
  ;; TODO edebug
  ;; TODO registers
  ;; Do we need dired? (d)
  ;; next errro (`)

  ;; h, j, l free
  ;; but k is kill buffer
  ;; ; :  undefined
  ;; [ { } ] are undefined
  ;; / \ is undefined
  ;; , is undefined

  ;; We make a number of keys available.
  (unbind-keys ("<f1>"      ; help
                "<f2>"      ; 2C-command
                "<f10>"     ; menu-bar-open
                "<f11>"     ; toggle-frame-fullscreen
                "<insert>"  ; overwrite-mode
                "<home>"    ; begin of line
                "<end>"     ; end of line
                "C-j"       ; electric-newline-and-maybe-indent
                "C-l"       ; recenter top-bottom
                "C-/" "C-_" ; undo
                "C-?"       ; undo-redo
                "C-@")       ; set-mark

               global-map)

  (unbind-keys ("C-d" "C-n" "C-o" "C-p" "C-q" "C-r" "C-x" "C-z"
                "C-@" "C-+" "C--" "C-0" "C-=" "C-;" "C-SPC"
                "M-g"
                "#" "$"        "\'" "(" ")"
                ;; "!" "\""        "%" "&"
                "1" "2" "3" "4" "5" "6"     "8"     "0" "-" "=" "^"
                ;;                      "7"     "9"         "~"  "\\" "|"
                "q"      "e" "r" "t"     "u" "i" "o"    "`" "@" "{"  "["
                ;;  "w"              "y"
                "a" "s"     "f" "g" "h"        "l" ";" "+" "*"  "]" "}"
                ;;                      "j"                ":"
                "z" "x" "X"         "m" "<" ">"     "\\")
                ;;          c            ,   .  ?/    _

               ctl-x-map))

;; Keep packages from dumping into Emacs config directory.
(use-package no-littering
  :config
  (no-littering-theme-backups))

;; Garbage Collection strategy to minimize GC interference.
(use-package gcmh
  :custom
  (gcmh-idle-delay 5)
  (gcmh-high-cons-threshold (* 32 1024 1024))
  :config
  (gcmh-mode +1))

;; Native compilation.
(use-feature comp
  :custom
  (native-comp-async-report-warnings-errors nil))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; Display
;;;
;;; Configuration:

(use-feature emacs
  :custom
  ;; Remove audible bell and reduce visual flash.
  (ring-bell-function #'flash-mode-line)
  :config
  (column-number-mode +1))

(use-feature frame
  :config
  ;; Remove visual trash.
  (window-divider-mode -1)
  (blink-cursor-mode   -1))

;; TODO Move to package
(use-feature milk-line
  :config
  (milk-line-mode))

;; Flash where text modification occurs.
(use-package goggles
  :hook ((prog-mode text-mode) . goggles-mode))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; Font
;;;
;;; Configuration:

(set-fontset-font "fontset-standard" 'latin
                  (font-spec :family "Iosevka Milk"
                             :size eml-default-font-size))

(set-fontset-font "fontset-standard" 'han
                  (font-spec :family "Sarasa Term J"
                             :size eml-default-font-size))

(set-fontset-font "fontset-standard" 'kana
                  (font-spec :family "Sarasa Term J"
                             :size eml-default-font-size))

(add-to-list 'default-frame-alist `(font . "fontset-standard"))

(create-fontset-from-fontset-spec "-*-*-*-*-*-*-*-*-*-*-*-*-fontset-variable")

(set-fontset-font "fontset-variable" 'latin
                  (font-spec :family "Sarasa Gothic J"
                             :size eml-default-font-size))

(set-fontset-font "fontset-variable" 'han
                  (font-spec :family "Sarasa Gothic J"
                             :size eml-default-font-size))

(set-fontset-font "fontset-variable" 'kana
                  (font-spec :family "Sarasa Gothic J"
                             :size eml-default-font-size))

(set-face-attribute 'variable-pitch nil :font "fontset-variable" :fontset "fontset-variable")

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; Navigation/Search
;;;
;;; Configuration:

;; Jump to point.
(use-package avy
  :demand t
  :bind (("C-;" . avy-goto-char-2))
  :custom
  (avy-all-windows 'all-frames)
  (avy-style       'at-full)
  (avy-background  nil))

;; Jump to open link.
(use-package link-hint
  :demand t
  :after (avy)
  :bind (("C-:" . link-hint-open-link))
  :custom
  (link-hint-message nil)
  (link-hint-restore t))

;; Isearch replacement.
;; TODO Change command support
(use-package ctrlf
  :demand t
  :bind (("M-s p" . consult-ctrlf-history)
         :map ctrlf-minibuffer-mode-map
         ("C-;" . avy-ctrlf))
  :custom
  (ctrlf-default-search-style 'fuzzy)
  :config
  (ctrlf-mode +1)
  (require 'milk-ctrlf))

;; Isearch still has to be configured for use where ctrlf doesn't work.
(use-feature isearch
  :custom
  (isearch-wrap-pause 'no-ding))

;; Enable subword word-movement.
;; Use forward-/backward- sexpr for word movement.
(use-feature subword
  :config
  (global-subword-mode +1))


(use-package consult
  :bind (:map goto-map
              ("g"   . consult-goto-line)
              ("M-g" . consult-goto-line)
              ("o"   . consult-outline)
              ("m"   . consult-mark)
              ("k"   . consult-global-mark)
              ("i"   . consult-imenu)
              ("I"   . consult-imenu-multi)
         :map search-map
              ("l" . consult-line)
              ("L" . consult-line-multi)
              ("k" . consult-keep-lines)
              ("u" . consult-focus-lines)
              ("f" . consult-find)
              ("g" . consult-ripgrep)
              ("G" . consult-git-grep)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; Completion/Minibuffer
;;;
;;; Configuration:

(require 'milk-complete)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; Assistance
;;;
;;; Configuration:

;; Cleanup help map.
(unbind-keys ("g"  "t" "C-a" "C-c" "C-e" "C-f" "C-n" "C-o" "C-p" "C-t" "C-w")
             help-map)

(bind-keys :map help-map
           ("C-k" . describe-keymap)
           ("C-f" . describe-face))

;; Replace standard help docs with more helpful ones.
;; TODO https://github.com/Wilfred/helpful/pull/217
;; Added changes locally.
(use-package helpful
  :bind (([remap describe-function] . helpful-callable)
         ([remap describe-command]  . helpful-command)
         ([remap describe-variable] . helpful-variable)
         ([remap describe-key]      . helpful-key)
         ([remap describe-symbol]   . helpful-symbol)))

;; Do not grow minibuffer on presenting eldoc.
(use-feature eldoc
  :custom
  (eldoc-echo-area-use-multiline-p nil)
  (eldoc-echo-area-prefer-doc-buffer t))

(use-feature emacs
  :custom
  (suggest-key-bindings nil))

(use-package consult
  :bind (("C-h S" . consult-info)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; Buffer & Window & Frame Management
;;;
;;; Configuration:

(require 'milk-win)
(setq-default tab-bar-show nil)
(require 'milk-tab)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; Standard Editing
;;;
;;; Configuration:

(use-feature emacs
  :bind (([remap capitalize-word] . capitalize-dwim)
         ([remap downcase-word]   . downcase-dwim)
         ([remap upcase-word]     . upcase-dwim))
  :hook ((before-save . delete-trailing-whitespace))
  :custom
  (hscroll-margin                  0)
  (hscroll-step                    1)
  (scroll-preserve-screen-position t)
  (scroll-margin                   0)
  (scroll-conservatively           10)
  (truncate-lines                  t)

  (treesit-extra-load-path  `(,(locate-user-emacs-file "local/treesit/")))

  (delete-trailing-lines     nil)
  (indent-tabs-mode          nil)
  (tab-width                 4)
  (tabify-regexp             "^\t* [ \t]+")
  (require-final-newline     t)
  (inhibit-eol-conversion    t)

  (sentence-end-double-space nil)
  (fill-column               120)

  (large-file-warning-threshold  100000000)
  :config
  (defalias 'yes-or-no-p 'y-or-n-p)
  (put 'narrow-to-region 'disabled nil))

(use-package undo-fu
  :bind (("C-z"   . undo-fu-only-undo)
         ("C-S-z" . undo-fu-only-redo))
  :custom
  (undo-limit        67108864)   ; 64mb.
  (undo-strong-limit 100663296)  ; 96mb.
  (undo-outer-limit  1006632960) ; 960mb.
  (undo-fu-ignore-keyboard-quit t))

(use-package vundo)

(use-package wgrep)

(use-package consult
  :bind (("M-y" . consult-yank-pop)

         ("M-;" . consult-register-store)
         ("M-:" . consult-register-load)

         ("C-M-;" . bookmark-set)
         ("C-M-:" . consult-bookmark)

         ("C-x C-k k" . consult-kmacro)))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; templates
;;;
;;; Configuration:

(use-package tempel
;; TODO
;;  :hook (((text-mode prog-mode) . tempel-setup-capf-complete)
;;         (LaTeX-mode . tempel-setup-capf-expand))
;;  :bind (("M-+" . tempel-complete)
;;         ("M-*" . tempel-insert))
  :custom
;; TODO Extend per major-mode
  (tempel-path (locate-user-emacs-file "local/templates"))
  (tempel-trigger-prefix "\\")
  (tempel-user-elements '(tempel-include))
  (tempel-mark nil)
;; TODO customize-face
  :init

  (defun tempel-setup-capf-expand ()
    (setq-local completion-at-point-functions
                (cons #'tempel-expand completion-at-point-functions)))
  (defun tempel-setup-capf-complete ()
    (setq-local completion-at-point-functions
                (cons #'tempel-complete completion-at-point-functions)))

  :config
  ;; Template elements
  (defun tempel-include (elt)
     (when (eq (car-safe elt) 'i)
       (if-let (template (alist-get (cadr elt) (tempel--templates)))
           (cons 'l template)
         (message "Template %s not found" (cadr elt))
         nil))))

;; TODO Replace
(use-package tempel-collection
  :after (tempel))

;; TODO (auto)yassnippet

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; Readers
;;;
;;; Configuration:

(use-package visual-fill-column
  :custom
  (visual-fill-column-center-text t)
  :config
  (advice-add 'text-scale-adjust :after #'visual-fill-column-adjust))

(require 'reading)

(use-package pdf-tools
  :mode ("\\.pdf\\'" . pdf-view-mode)
  :hook ((pdf-view-mode . (lambda () (ctrlf-local-mode -1))))
  :config
  (pdf-loader-install))

(use-package nov
  :mode ("\\.epub\\'" . nov-mode)
  :hook ((nov-mode . reading-mode))
  :custom
  (nov-variable-pitch t))

;; TODO
(use-feature info)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; Project Management
;;;
;;; Configuration:

;; TODO config project

(use-package magit
  ;; TODO More
  :bind (:map vc-prefix-map
              ("P" . magit-push)
              ("f" . magit-file-dispatch)
              ("g" . magit-dispatch)
              ("s" . magit-status)
              ("d" . magit-diff-dwim)
              ("e" . magit-ediff-dwim)
              ("F" . magit-pull))
  :custom
  (magit-diff-refine-hunk           t)
  (magit-define-global-key-bindings nil)
  (magit-save-repository-buffers    nil))

(use-package magit-todos
  :after magit
  :config
  (magit-todos-mode))

;; TODO move to theme
(use-package hl-todo
  :hook ((prog-mode . hl-todo-mode))
  :custom
  (hl-todo-keyword-faces
        `(("TODO"  . ,(face-foreground 'warning))
          ("FIXME" . ,(face-foreground 'error))
          ("NOTE"  . ,(face-foreground 'success))
          ("XXX"   . ,(face-foreground 'success)))))

(use-package consult-git-log-grep
  :bind (:map vc-prefix-map
              ("L" . consult-git-log-grep))
  :custom
  (consult-git-log-grep-open-function #'magit-show-commit))

(use-package consult-project-extra)
  ;; TODO bind

(use-feature vc
  :custom
  (vc-follow-symlinks t))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; Language
;;;
;;; Configuration:

(use-package jinx
  :init
  (global-jinx-mode))

;; TODO dicts
;; TODO translate

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; Org
;;;
;;; Configuration:

;; TODO binds, locations
(use-package org
  :hook ((org-mode . variable-pitch-mode)
         (org-mode . org-cdlatex-mode))
  :bind (("M-o c" . org-capture)
         ("M-o l" . org-store-link)
         :map org-mode-map
         ("C-c p" . (lambda () (interactive) (org-latex-export-to-pdf t))))
  :custom
  (org-agenda-files '("~/txt/"))
  (org-todo-keywords '((sequence "TODO" "PROVE" "REVIEW" "|" "DONE" "DISCARDED")))
  (org-auto-align-tags nil)
  (org-catch-invisible-edits nil)
  (org-tags-column 0)
  (org-special-ctrl-a/e t)
  (org-insert-heading-respect-content t)
  (org-hide-emphasis-markers t)
  (org-pretty-entities t)
  (org-ellipsis "…")
  (org-directory "~/txt/org/")
  (org-default-notes-file "~/txt/org/notes.org")
  (org-export-in-background nil)
  (org-confirm-babel-evaluate nil)
;TODO  (org-cite-global-bibliography `(,(expand-file-name eml-main-bib-file)))
  (org-cite-export-processors '((latex biblatex) (t basic)))
  (org-babel-load-languages '((emacs-lisp . t)
                              (C . t)
                              (eshell . t)
                              (scheme . t)))
  (org-capture-templates
   '(
     ("v" "Vocabulary" entry (file+olp "~/txt/anki.org" "Mandarin"  "Vocabulary")
      "*** Item\n:PROPERTIES:\n:ANKI_NOTE_TYPE: Basic (and reversed card)\n:END:\n**** Front\n%i\n**** Back\n%?\n"
      :prepare-finalize (lambda () (unless org-note-abort (anki-editor-push-notes))))))

  :config
  (require 'ox-latex)
  (require 'ox)
;;  (setq  org-export-async-init-file nil)

  (add-to-list 'org-latex-classes
               '("acmart" "\\documentclass[10pt]{acmart}"
                 ("\\section{%s}"       . "\\section*{%s}")
                 ("\\subsection{%s}"    . "\\subsection*{%s}")
                 ("\\subsubsection{%s}" . "\\subsubsection*{%s}")
                 ("\\paragraph{%s}"     . "\\paragraph*{%s}")
                 ("\\subparagraph{%s}"  . "\\subparagraph*{%s}")))

  (setq org-latex-inputenc-alist '(("utf8" . "utf8")))
  (setq org-latex-pdf-process
      '("%latex -shell-escape -bibtex -interaction nonstopmode -output-directory %o -bibtex -pdf %f"
        "%latex -shell-escape -interaction nonstopmode -output-directory %o -bibtex -pdf %f"
        "%latex -shell-escape -interaction nonstopmode -output-directory %o -bibtex -pdf %f"))

  (defun org-export-async-revert (source backend &optional process)
    (when (stringp source)
      (let ((buf (get-file-buffer source)))
        (when buf
          (save-current-buffer
            (set-buffer buf)
            (revert-buffer nil t))))))
  (advice-add 'org-export-add-to-stack :after #'org-export-async-revert))

(use-package htmlize)

(use-package org-modern
  :after (org)
  :hook (((org-mode org-agenda-finalize) . org-modern-mode)))

;; TODO binds
(use-package org-auctex
  :straight (org-auctex :type git :host github :repo "karthink/org-auctex")
  :hook ((org-mode . org-auctex-mode))
  :bind (:map org-mode-map
              ("C-c C-x C-l" . org-auctex-preview-dwim)))

;; TODO binds
(use-package org-noter)


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; (De)Notes
;;;
;;; Configuration:

(use-package denote
  :demand t
  :custom
  (denote-directory                 (expand-file-name eml-denote-directory))
  (denote-file-type                 'org)
  ;; TODO Combined with ebib-keywords
  (denote-known-keywords            '())
  (denote-sort-keywords             t)
  (denote-infer-keywords            t)
  (denote-allow-multi-word-keywords nil)
  (denote-prompts                   '(title keywords))
  (denote-dired-directories         nil)
  (denote-date-format               nil)
  (denote-backlinks-show-context    nil)

  (denote-date-prompt-use-org-read-date t))

(use-package consult-notes
  :demand t
  :after (denote)
  ;; bind consult-notes
  ;; consult-multi ???
  :config
  (consult-notes-denote-mode +1))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; Study
;;;
;;; Configuration:

(use-package gptel
  :custom
  (gptel-api-key ""))

(use-package anki-editor)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; References
;;;
;;; Configuration:

(require 'milk-bib)

(use-package citar-denote
  :demand t
  :after (citar denote)
  :custom
  (citar-denote-subdir t)
  :config
  (citar-denote-mode +1))

(use-package citar-embark
  :demand t
  :after (citar embark)
  :config
  (citar-embark-mode +1))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; Markdown
;;;
;;; Configuration:

(use-package markdown-mode)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; Tex
;;;
;;; Configuration:

(use-feature tex
  :mode ("\\.tex\\'" . TeX-latex-mode)
  :hook ((LaTeX-mode . prettify-symbols-mode)
         (TeX-after-compilation-finished-functions . TeX-revert-document-buffer))
  :custom
  (TeX-auto-save              t
   (TeX-save-query             nil)
   (TeX-parse-self             t)
   (TeX-show-compilation       nil)
   (TeX-view-program-selection '((output-pdf "PDF Tools")))
   (TeX-view-program-list      '(("PDF Tools" TeX-pdf-tools-sync-view)))
   (TeX-command-extra-options  "-shell-escape")
   (TeX-source-correlate-start-server t)))

(use-feature preview
  :after tex
  :custom
  (preview-scale-function #'preview-scale-from-face))

(use-package cdlatex
  :hook (LaTeX-mode . cdlatex-mode))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; Programming
;;;
;;; Configuration:

;; TODO binds
(use-package eglot
  :config
  (add-to-list 'eglot-stay-out-of 'flymake)
  (defun eglot-try-actions-at-point (action-titles)
    "Attempt to execute the ACTION-TITLES at point in order until success."
    (let* ((actions (eglot-code-actions
                     (car (eglot--region-bounds))
                     (cadr (eglot--region-bounds))
                     nil nil))
           (available (cl-loop for a in actions
                               collect (cons (plist-get a :title) a)))
           (chosen  (seq-some (lambda (elt) (assoc-string elt available)) action-titles)))
      (when chosen (eglot-execute (eglot--current-server-or-lose) (cdr chosen))))))

(use-package eglot-tempel
  :after (eglot tempel)
  :hook  ((eglot-managed-mode . eglot-tempel-mode)))

;; TODO binds
(use-package flycheck
  :hook ((prog-mode . flycheck-mode))
  :custom
  (flycheck-mode-line-prefix nil)
  (flycheck-display-errors-function #'flycheck-display-error-messages-truncated)
  :config
  (defun flycheck-display-error-messages-truncated (errors)
    (when (and errors (flycheck-may-use-echo-area-p))
      (let ((message (string-replace "\n" " " (flycheck-help-echo-all-error-messages errors)))
            (space   (- (window-total-width) 5)))
        (display-message-or-buffer
         (if (length< message space)
             message
           (format "%s%s" (substring message 0 (- space (length "…"))) "…"))
         flycheck-error-message-buffer
         'not-this-window)
        (-when-let (buf (get-buffer flycheck-error-message-buffer))
          (with-current-buffer buf
            (unless (derived-mode-p 'flycheck-error-message-mode)
              (flycheck-error-message-mode))))))))

(use-package flycheck-eglot
  :after (flycheck eglot)
  :hook ((eglot-managed-mode . flycheck-eglot-mode)))

(use-package rainbow-mode)

(use-package consult-eglot
  :after (consult eglot)
  :bind (:map eglot-mode-map
              ("M-g e" . consult-eglot-symbols)))

(use-package consult-flycheck
  :after (consult flycheck)
  :bind (:map flycheck-mode-map
              ("M-g f" . consult-flycheck)))

(use-feature hl-line
  :hook ((prog-mode . hl-line-mode)))

(use-feature paren
  :hook ((prog-mode . show-paren-mode))
  :custom
  (show-paren-style              'parenthesis)
  (blink-matching-paren-distance 100000))

(use-feature xref
  :custom
  (xref-search-program 'ripgrep))

(use-feature electric
  :config
  (electric-indent-mode -1))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; Emacs Lisp
;;;
;;; Configuration:

;; TODO bind
(use-package macrostep
  :bind (:map emacs-lisp-mode-map
              ("C-c e" . #'macrostep-expand)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; C/C++
;;;
;;; Configuration:

;; TODO proper formatter configuration
(use-package cc-mode
  :custom
  (c-default-style "linux")
  (c-basic-offset  2)
  (c-offsets-alist '((innamespace . 0))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; ATS
;;;
;;; Configuration:

(use-package ats-mode
  :straight (ats-mode :type git
                      :host github
                      :repo "githwxi/ATS-Postiats"
                      :files ("utils/emacs/*.el")
                      :nonrecursive t)
  :mode (("\\.[sdh]ats\\'" . ats-mode))
  :demand t
  :config
  (flycheck-ats2-setup)
  (setenv "PATSHOME" "/usr/local/lib/ats2-postiats-0.4.2"))

(use-feature ob-ats2
  :after (org ats-mode)
  :demand t
  :config
  (add-to-list 'org-babel-load-languages '(ats2 . t))
  (add-to-list 'org-src-lang-modes '("ats2" . ats)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; Rust
;;;
;;; Configuration:

(use-package rustic
  ;; :mode Autoloaded
  :custom
  (rustic-format-on-save nil))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; OCaml
;;;
;;; Configuration:

(use-package utop
  :bind (:map utop-mode-map
              ("<up>" . utop-history-goto-prev)
              ("<down>" . utop-history-goto-next))
  :custom
  (utop-command "opam exec -- utop -emacs"))

(use-package tuareg
  ;; :mode Autoloaded
  :hook ((tuareg-mode . utop-minor-mode))
  :bind (:map eglot-mode-map
              ("C-c C-c" . ocamllsp-destruct-at-point)
              ("C-c C-d" . ocamllsp-infer-at-point)
              ("C-x p i" . ocamllsp-create-interface))
  :config
  ;; TODO Typed hole jump
  ;; https://github.com/ocaml/ocaml-lsp/blob/master/ocaml-lsp-server/docs/ocamllsp/typedHoles-spec.md
  (defun ocamllsp-destruct-at-point ()
    (interactive)
    (eglot-try-actions-at-point '("Destruct")))
  (defun ocamllsp-infer-at-point ()
    (interactive)
    (eglot-try-actions-at-point `("Type-annotate")))
  (defun ocamllsp-create-interface ()
    (interactive)
    (eglot-try-actions-at-point `(,(concat "Create " (buffer-name) "i"))))

  ;; (defun ocamllsp-goto-hole ()
  ;;   (interactive)
  ;;   (eglot-execute (eglot--current-server-or-lose)
  ;;                  `(:command
  ;;                    (:title "typedHoles"
  ;;                     :command "ocamllsp/typedHoles"
  ;;                     :arguments [,(eglot--path-to-uri buffer-file-name)])
  ;;                        )) )
  )

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; Lisp
;;;
;;; Configuration:

(use-package sly
  :bind (:map sly-editing-mode-map
              ([remap display-local-help]        . sly-describe-symbol)
              ([remap embark-pp-eval-defun]      . sly-compile-defun)
              ([remap pp-macroexpand-expression] . sly-expand-1)
              ([remap pp-eval-expression]        . sly-interactive-eval)
              ([remap xref-find-definitions]     . sly-edit-definition)))

(use-package sly-macrostep
  :after (sly)
  :demand t)

(use-package sly-quicklisp
  :after (sly)
  :demand t)

;; TODO
(add-to-list 'Info-directory-list (expand-file-name "~/txt/doc/info/common-lisp/"))

(use-package paredit
  :hook ((emacs-lisp-mode lisp-mode scheme-mode) . paredit-mode))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; Scheme
;;;
;;; Configuration:

(use-package scheme
  :ensure t)

(use-package geiser
  :hook ((scheme-mode . geiser-mode))
  :bind (:map scheme-mode-map
              ("C-." . embark-act)
              ([remap display-local-help]        . geiser-doc-symbol-at-point)
              ([remap embark-pp-eval-defun]      . geiser-eval-definition)
  ;TODO           ([remap pp-macroexpand-expression] . geiser-expand-last-sexp)
  ;TODO               ([remap pp-eval-expression]        . geiser-eval-region)
              )
  :custom
  (geiser-active-implementations    '(chez chicken))
  (geiser-autodoc-identifier-format "%s → %s"))

(use-package geiser-chez
  :after (geiser))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; Agda
;;;
;;; Configuration:

(use-feature agda2-mode
  :load-path
  (lambda () (let ((coding-system-for-read 'utf-8))
               (file-name-directory
                (shell-command-to-string "agda-mode locate"))))
  :mode ("\\.l?agda\\'" . agda2-mode)
  :hook ((agda2-mode . (lambda () (activate-input-method "Agda"))))
  :custom
  (agda2-information-window-max-height 0.25))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; Lean
;;;
;;; Configuration:

(use-package lean4-mode
  :straight (lean4-mode :type git :host github :repo "leanprover/lean4-mode"
                        :files ("*.el" "data"))
  :commands (lean4-mode))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; Coq
;;;
;;; Configuration:

(use-package proof-general
  :custom
  (coq-compile-before-require t)
  (overlay-arrow-string ""))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; Terminal
;;;
;;; Configuration:

(use-package vterm
  :bind (:map vterm-mode-map
              ("C-q" . vterm-send-next-key))
  :hook (vterm-mode . (lambda () (ctrlf-local-mode -1)))
  :config
  (unbind-keys ("C-@" "M-@" "C-M-@" "C-`") vterm-mode-map))

(use-package multi-vterm
  :after (vterm))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; Browsers
;;;
;;; Configuration:

(use-feature browse-url
  :custom
  (browse-url-browser-function           'eww-browse-url)
  (browse-url-secondary-browser-function 'browse-url-firefox))

(use-feature url
  :custom
  (url-privacy-level '(email cookies lastloc)))

(use-feature shr
  :custom
  (shr-use-colors     nil)
  (shr-bullet         "• ")
  (shr-folding-mode   t)
  (shr-inhibit-images t))

(use-feature eww
  :custom
  (eww-search-prefix      "https://html.duckduckgo.com/html?q=")
  (eww-download-directory eml-download-directory)
  (eww-header-line-format nil))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; Notmuch
;;;
;;; Configuration:

;; XXX Bad to use :build, but better than keeping this inside system package
;; manager or some other hack since we only intend to work inside Emacs.
(use-package notmuch
  :straight
  (notmuch :type git :repo "https://git.notmuchmail.org/git/notmuch"
            :pre-build (("./configure" "--without-bash-completion" "--without-ruby" "--without-zsh-completion")
                        ("make"))
            :files ("emacs/*.el"))
  :hook ((notmuch-hello-mode . notmuch-new))
  :bind (("<XF86Explorer>" . notmuch-hello))
  :custom
  (notmuch-command (expand-file-name "notmuch" (straight--repos-dir "notmuch")))
  (notmuch-search-oldest-first nil)
  :config
  (defun notmuch-new ()
    (interactive)
    (async-shell-command (concat notmuch-command " new"))))

(use-feature emacs
  :custom
  (user-full-name "Zachery Casey")
  (user-mail-address "caseyz@bu.edu"))

(use-feature sendmail
  :custom
  (sendmail-program "msmtp")
  (send-mail-function #'sendmail-send-it))
(use-feature loaddefs
  :custom
  (mail-specify-envelope-from t)
  (mail-envelope-from         'header)
  (mail-user-agent 'message-user-agent))

(use-feature message
  :custom
  (message-sendmail-envelope-from 'header)
  (message-sendmail-extra-arguments '("--read-envelope-from")))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; Elfeed
;;;
;;; Configuration:

(use-package elfeed
  :bind (("<XF86LaunchA>" . elfeed))
  :custom
  (elfeed-feeds
   '(
     ("https://planet.emacslife.com/atom.xml" emacsa)
     ("https://queuea9.wordpress.com/feed/" blog type-theory)
     ("https://feeds.buzzsprout.com/728558.rss" podcast type-theory)
     ("http://planet.lisp.org/rss20.xml" lisp development programming)
     ("https://planet.scheme.org/atom.xml" scheme development programming))))


(use-package bongo
  :custom
  (bongo-custom-backend-matchers
   '((mpv (local-file "http:" "https:" "ftp:") . t))))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; Emacs Libraries
;;;
;;; Configuration:

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; Misc.
;;;
;;; Configuration:


(use-feature savehist
  :config
  (savehist-mode +1))

(use-feature recentf
  :custom
  (recentf-max-menu-items  100)
  (recentf-max-saved-items 100)
  :config
  (recentf-mode))

(use-feature dired
  :custom
  (dired-auto-revert-buffer t)
  (dired-dwim-target        t)
  (dired-recursive-copies  'always)
  (dired-recursive-deletes 'top)

  (dired-hide-details-hide-symlink-targets nil))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;




;; (use-package bind-key

;;  :bind* (;; Add the global keybindings for accessing Org Agenda and
;;          ;; Org Capture that are recommended in the Org manual.
;;          ("C-c a" . #'org-agenda)
;;          ("C-c c" . #'org-capture))
;; (defvar radian-keymap (make-sparse-keymap)
;;   "Keymap for Radian commands that should be put under a prefix.
;; This keymap is bound under \\[radian-keymap].")
;; (bind-key* "M-P" radian-keymap)

;;  (pdf-view-midnight-colors '("#2f2241" . "#ead6b6")) ;; TODO MOVE TO THEME

;;




;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Navigation

;; (use-package consult
;;       ("C-x M-:" . consult-complex-command)

;;          ("C-c m"             . consult-man)

;;       ("C-x p b" . consult-project-buffer)
;;          ("C-x p e" . consult-compile-error)



;;
;;
;;
;;          ("M-s e" . consult-isearch-history)

;;          :map isearch-mode-map
;;               ("M-e"   . consult-isearch-history)
;;               ("M-s e" . consult-isearch-history)
;;            ("M-s l" . consult-line)
;;            ("M-s L" . consult-line-multi)))
;;



;;; init.el ends here
