;;; early-init.el -*- lexical-binding: t; -*-

;;; Code:

(defvar gc-cons-threshold-old gc-cons-threshold)
(defvar file-name-handler-alist-old file-name-handler-alist)

(setq gc-cons-threshold       most-positive-fixnum
      file-name-handler-alist nil)

(add-hook 'emacs-startup-hook
          (lambda ()
            (setq gc-cons-threshold       gc-cons-threshold-old
                  file-name-handler-alist file-name-handler-alist-old)))


(setq load-prefer-newer         t
      package-enable-at-startup nil
      user-emacs-directory      (file-name-directory load-file-name))

(setq default-frame-alist
      '((menu-bar-lines . 0)
	    (tool-bar-lines . 0)
        (left-fringe . 0)
        (right-fringe . 0)
        (no-special-glyphs . t)
	    (horizontal-scroll-bars . nil)
	    (vertical-scroll-bars . nil)))

(setq initial-scratch-message nil
      fancy-startup-text      nil
      fancy-about-text        nil
      site-run-file           nil
      fancy-splash-image      (concat user-emacs-directory "splash.png"))

(setq custom-theme-directory (concat user-emacs-directory "local"))
(load-theme 'all-is-one t)

(setq frame-resize-pixelwise            t
      frame-inhibit-implied-resize      t
      inhibit-startup-screen            nil
      inhibit-startup-echo-area-message user-login-name
      inhibit-default-init              t
      initial-major-mode               'fundamental-mode
      initial-scratch-message           nil
      frame-inhibit-implied-resize      t)

(advice-add #'display-startup-echo-area-message :override #'ignore)
(advice-add #'x-apply-session-resources :override #'ignore)

(setq-default bidi-display-reordering 'left-to-right
              bidi-paragraph-direction 'left-to-right
	      cursor-in-non-selected-windows nil)

(setq auto-mode-case-fold           nil
      highlight-nonselected-windows nil
      bidi-inhibit-bpa              t
      fast-but-imprecise-scrolling  t
      idle-update-delay             1.0)

;;; early-init.el ends here

