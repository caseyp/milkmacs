;;; ob-ats2.el --- org-babel functions for ATS2 evaluation

;; Copyright (C) Zachery Casey

;; Author: Zachery Casey
;; Keywords: literate programming, reproducible research
;; Homepage: https://orgmode.org
;; Version: 0.01

;;; License:

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation; either version 3, or (at your option)
;; any later version.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with GNU Emacs; see the file COPYING.  If not, write to the
;; Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
;; Boston, MA 02110-1301, USA.

;;; Commentary:

;; After that continue by creating a simple code block that looks like e.g.
;;
;; #+begin_src ats2 :main yes :includes '(share/atspre_staload.hats)

;; val _ = print("Hello, world!")

;; #+end_src

;; Finally you can use `edebug' to instrumentalize
;; `org-babel-expand-body:ats2' and continue to evaluate the code block. You
;; try to add header keywords and change the body of the code block and
;; reevaluate the code block to observe how things get handled.

;;
;; If you have questions as to any of the portions of the file defined
;; below please look to existing language support for guidance.
;;
;; If you are planning on adding a language to org-babel we would ask
;; that if possible you fill out the FSF copyright assignment form
;; available at https://orgmode.org/request-assign-future.txt as this
;; will make it possible to include your language support in the core
;; of Org-mode, otherwise unassigned language support files can still
;; be included in the contrib/ directory of the Org-mode repository.

;;; Code:
(require 'ob)
(require 'ob-ref)
(require 'ob-comint)
(require 'ob-eval)

(require 'ats-mode)

;; optionally define a file extension for this language
(add-to-list 'org-babel-tangle-lang-exts '("ats2" . "dats"))

(defvar org-babel-default-header-args:ats2 '())

(defconst org-babel-header-args:ats2 '((includes . :any)
				       (defines  . :any)
				       (main     . :any)
				       (flags    . :any)
				       (cmdline  . :any)
				       (libs     . :any))
  "ATS2-specific header arguments.")

(defcustom org-babel-ats2-compiler "patscc -cleanaft"
  "Command used to compile an ATS2 source code file into an executable.
May be either a command in the path, like patscc
or an absolute path name, like /usr/local/bin/patscc
parameter may be used, like patscc -DATS"
  :group 'org-babel
  :version "24.3"
  :type 'string)

(defcustom org-babel-ats2-default-includes '(prelude/lmacrodef.hats
                                             share/atspre_define.hats
                                             share/atspre_staload.hats)
  "Files to have automatically #include-d by default in the source code file."
  :group 'org-babel
  :version "24.3"
  :type '(list symbol))

(defcustom org-babel-ats2-default-flags '("-DATS_MEMALLOC_LIBC")
  "Flags to include by default to pass to the compiler."
  :group 'org-babel
  :version "24.3"
  :type '(list string))


;; This function expands the body of a source code block by doing things like
;; prepending argument definitions to the body, it should be called by the
;; `org-babel-execute:ats2' function below. Variables get concatenated in
;; the `mapconcat' form, therefore to change the formatting you can edit the
;; `format' form.
(defun org-babel-expand-body:ats2 (body params &optional processed-params)
  "Expand BODY according to PARAMS, return the expanded body."
  (let ((vars (org-babel--get-vars (or processed-params (org-babel-process-params params))))
	(main-p (not (string= (cdr (assq :main params)) "no")))
	(includes (org-babel-read
		   (cdr (assq :includes params))
		   nil)))
    (when (stringp includes)
      (setq includes (split-string includes)))

    (concat
     (mapconcat ;; define any variables
      (lambda (pair)
        (fpormat "%s=%S"
                (car pair) (org-babel-ats2-var-to-ats2 (cdr pair))))
      vars "\n")
     (mapconcat
      (lambda (inc)
	;; :includes '(foo bar) gives us a list of
	;; symbols; convert those to strings.
	    (when (symbolp inc) (setq inc (symbol-name inc)))
	    (format "#include \"%s\"" inc))
      (append org-babel-ats2-default-includes includes) "\n")
     "\n"
     (if main-p
	 (org-babel-ats2-ensure-main-wrap body)
       body)
     "\n")))

(defun org-babel-ats2-ensure-main-wrap (body)
  "Wrap BODY in a \"main\" function call if none exists."
  (if (string-match "^[ \t]*implement[ \t\n\r]*main0[ \t]*(.*)" body)
      body
    (format "%s\nimplement main0 () = ()\n" body)))

;; This is the main function which is called to evaluate a code
;; block.
;;
;; This function will evaluate the body of the source code and
;; return the results as emacs-lisp depending on the value of the
;; :results header argument
;; - output means that the output to STDOUT will be captured and
;;   returned
;; - value means that the value of the last statement in the
;;   source code block will be returned
;;
;; The most common first step in this function is the expansion of the
;; PARAMS argument using `org-babel-process-params'.
;;
;; Please feel free to not implement options which aren't appropriate
;; for your language (e.g. not all languages support interactive
;; "session" evaluation).  Also you are free to define any new header
;; arguments which you feel may be useful -- all header arguments
;; specified by the user will be available in the PARAMS variable.
(defun org-babel-execute:ats2 (body params)
  "Execute a block of ATS2 code with org-babel.
This function is called by `org-babel-execute-src-block'"
  (message "executing ATS2 source code block")
  (let* ((tmp-src-file (org-babel-temp-file "ATS2-src-" ".dats"))
	 (tmp-bin-file (org-babel-process-file-name
			(org-babel-temp-file "ATS2-bin-" org-babel-exeext)))
	 (cmdline (cdr (assq :cmdline params)))
	 (cmdline (if cmdline (concat " " cmdline) ""))
	 (flags (append org-babel-ats2-default-flags (cdr (assq :flags params))))
	 (flags (mapconcat 'identity
			           (if (listp flags) flags (list flags)) " "))
	 (libs (org-babel-read
		(or (cdr (assq :libs params))
		    (org-entry-get nil "libs" t))
		nil))
	 (libs (mapconcat #'identity
			  (if (listp libs) libs (list libs))
			  " "))
	 (processed-params (org-babel-process-params params))
         ;; variables assigned for use in the block
         (vars (org-babel--get-vars processed-params))
         (result-params (assq :result-params processed-params))
         ;; either OUTPUT or VALUE which should behave as described above
         (result-type (assq :result-type processed-params))
         ;; expand the body with `org-babel-expand-body:ats2'
         (full-body (org-babel-expand-body:ats2
                     body params processed-params)))
    (with-temp-file tmp-src-file (insert full-body))
    (org-babel-eval (format "%s -o %s %s %s %s"
			    org-babel-ats2-compiler
			    tmp-bin-file
			    flags
			    (org-babel-process-file-name tmp-src-file)
			    libs)
		    "")
    (let ((results (org-babel-eval (concat tmp-bin-file cmdline) "")))
      (when results
	(setq results (org-remove-indentation results))
	(org-babel-reassemble-table
	 (org-babel-result-cond (cdr (assq :result-params params))
	   results
	   (let ((tmp-file (org-babel-temp-file "c-")))
	     (with-temp-file tmp-file (insert results))
	     (org-babel-import-elisp-from-file tmp-file)))
	 (org-babel-pick-name
	  (cdr (assq :colname-names params)) (cdr (assq :colnames params)))
	 (org-babel-pick-name
	  (cdr (assq :rowname-names params)) (cdr (assq :rownames params))))))))

;; This function should be used to assign any variables in params in
;; the context of the session environment.
(defun org-babel-prep-sessioppn:ats2 (session params)
  "Prepare SESSION according to the header arguments specified in PARAMS."
  (error "ATS2 is a compiled language -- no support for sessions"))

;; TODO
(defun org-babel-ats2-var-to-ats2 (var)
  "Convert an elisp var into a string of ats2 source code
specifying a var of the same value."
  (format "%S" var))

;; TODO
(defun org-babel-ats2-table-or-string (results)
  "If the results look like a table, then convert them into an
Emacs-lisp table, otherwise return the results as a string."
  )


(defun org-babel-ats2-initiate-session (&optional session)
  "If there is not a current inferior-process-buffer in SESSION then create.
Return the initialized session."
  (error "ATS2 is a compiled language -- no support for sessions"))

(provide 'ob-ats2)
;;; ob-ats2.el ends here
