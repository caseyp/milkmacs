;;; milk-config.el -- General configuration -*- lexical-binding: t -*-
;;; Commentary:
;;; Code:

(defgroup eml-config nil
  "Emilks configuration."
  :link '(url-link :tag "Homepage" "https://chocolateymilk.mooo.com/git/milk/emilks")
  :prefix "eml-")

(defcustom eml-default-font-size 13
  "Default font size."
  :type '(choice integer (const :tag "Default" 13)))

(defcustom eml-denote-directory "~/txt/"
  "Directory where notes can be stored."
  :type '(choice stringp (const :tag "Default" "~/txt/")))

(defcustom eml-download-directory "~/dl/"
  "Directory where things should be downloaded (browser, etc)."
  :type '(choice stringp (const :tag "Default" "~/dl/")))

(defgroup eml-bib nil
  "Emilks bibliography configuration."
  :prefix "eml-bib-")

(defcustom eml-bib-main-file "~/txt/ref.bib"
  "Main database for bibliographical references."
  :type '(choice stringp (const :tag "Default" "~/txt/ref.bib")))

(defcustom eml-bib-pdf-storage-directory "~/txt/papers/"
  "Directory for storing documents."
  :type '(choice stringp (const :tag "Default" "~/txt/papers/")))

(defcustom eml-bib-scihub-homepage "https://sci-hub.se"
  "Which sci-hub mirror to use."
  :type '(choice stringp (const :tag "Default" "https://sci-hub.se" )))

(provide 'milk-config)
;;; milk-config.el ends here
