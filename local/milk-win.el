;;; milk-win.el -- Window management -*- lexical-binding: t -*-
;;; Commentary:
;;; Code:

(require 'bind-key)

(bind-keys* :prefix-map window-management-map
            :prefix "C-j"
            ("o" . other-window)
            ("0" . delete-window)
            ("1" . delete-other-windows)
            ("+" . balance-windows)
            (";" . fit-window-to-buffer)
            ("-" . shrink-window-if-larger-than-buffer)
            ("a" . minimize-window)
            ("e" . maximize-window)
            ("h" . split-window-horizontally)
            ("v" . split-window-vertically)
            ("^" . enlarge-window)
            ("~" . enlarge-window-horizontally))
;; TODO Place other-window-prefix closer?
(bind-key "4" 'ctl-x-4-prefix  window-management-map)

(bind-keys* :prefix-map frame-management-map
            :prefix "C-l"
            ("o" . other-frame)
            ("0" . delete-frame)
            ("1" . delete-other-frames)
            ("2" . make-frame-command)
            ("3" . tear-off-window)
            ("5" . ctl-x-5-map)
            ("c" . clone-frame))
;; TODO Place other-frame-prefix closer?
(bind-key "5" 'ctl-x-5-prefix  frame-management-map)

;; Allow restoring previous window configurations.
(use-feature winner
  :demand t
  :bind* (:map window-management-map
               ("z" . winner-undo)
               ("Z" . winner-redo))
  :custom
  (winner-dont-bind-my-keys t)
  :config
  (winner-mode +1))

;; Move between windows directionally, rather than with other-window.
(use-feature windmove
  :demand t
  :bind* (:map window-management-map
               ("b" . windmove-left)
               ("C-b" . windmove-left)
               ("f" . windmove-right)
               ("C-f" . windmove-right)
               ("p" . windmove-up)
               ("C-p" . windmove-up)
               ("n" . windmove-down)
               ("C-n" . windmove-down)))

(use-package transpose-frame
  :demand t
  :bind (:map window-management-map
              ("t" . transpose-frame)))

(use-feature window
  :custom
  (window-resize-pixelwise t)
  (fit-window-to-buffer-horizontally t)
  (display-buffer-alist
   `((,(rx bos "*helpful" (* anychar) eos)
      (display-buffer-reuse-window display-buffer-in-side-window)
      (reusable-frames . visible)
      (side            . right)
      (window-width    . (lambda (win) (fit-window-to-buffer win nil nil 90))))
     (,(rx bos "*Embark" (* anychar) eos)
      (display-buffer-reuse-window display-buffer-in-side-window)
      (reusable-frames . visible)
      (side            . right)
      (window-width    . fit-window-to-buffer)))))

(use-package popper
  :demand t
  :bind (("C-@"   . popper-toggle-latest)
         ("M-@"   . popper-cycle)
         ("C-M-@" . popper-toggle-type)
         ("C-`"   . popper-kill-latest-popup))
  :custom
  (popper-reference-buffers '(("\\*Messages\\*" . hide)
                              ("\\*Warnings\\*" . hide)
                              ("Output\\*$" . hide)
                              ("\\*Async Shell Command\\*" . hide)
                              "\\*Geiser Documentation\\*"
                              "\\*Flycheck errors\\*"
                              "\\*vterminal - dedicated\\*"
                              embark-collect-mode
                              "\\`\\*Embark[^z-a]*\\'"
                              help-mode helpful-mode
                              (compilation-mode . hide)))
  (popper-group-function    nil)
  (popper-display-control   nil)
  (popper-echo-lines 1)
  :config
  (popper-mode +1)
  (popper-echo-mode +1))
;; TODO popper-echo-transform-function
;; TODO popper-group-function

(use-package consult
  :bind (("C-x b"   . consult-buffer)
         ("C-x j b" . consult-buffer-other-window)
         ("C-x l b" . consult-buffer-other-frame)))

(use-feature minibuffer
  :hook ((minibuffer-setup . cursor-intangible-mode))
  :custom
  (minibuffer-prompt-properties      '(read-only t cursor-intangible
                                                 t face minibuffer-prompt))
  (enable-recursive-minibuffers      t)
  (minibuffer-follows-selected-frame nil)
  :config
  (defun abort-minibuffer-if-active-advice (keyboard-quit)
    "Abort any active minibuffer or call KEYBOARD-QUIT."
    (if-let ((minibuffer (active-minibuffer-window)))
        (progn
          (switch-to-buffer (window-buffer minibuffer))
          (abort-minibuffers))
      (funcall keyboard-quit)))

  (advice-add #'keyboard-quit :around #'abort-minibuffer-if-active-advice))

(use-feature frame
  :config
  (defun invoke-frame-cleanup (frame)
    (when-let (funs (frame-parameter frame 'cleanup-frame-functions))
      (dolist (fn funs) (funcall fn frame))))

  (add-hook 'delete-frame-functions #'invoke-frame-cleanup)

  (defun kill-child-buffer (frame)
    "Kill the buffer associated to FRAME in child-buffer frame parameter.
The buffer is killed without a prompt."
    (let ((child-buffer (frame-parameter frame 'child-buffer)))
      (set-buffer child-buffer)
      (setq-local kill-buffer-query-functions nil)
      (and child-buffer (kill-buffer child-buffer)))))

(defun attach-buffer-to-frame (buffer)
  "Kill BUFFER when current frame is deleted."
  (interactive "bBuffer:\n")
  (set-frame-parameter nil 'cleanup-frame-functions
                       (cons (lambda (_) (let ((kill-buffer-query-functions nil))
                                           (kill-buffer buffer)))
                             (frame-parameter nil 'cleanup-frame-functions))))


(provide 'milk-win)
;;; milk-win.el ends here
