;;; milk-note.el -- Configuration for note taking -*- lexical-binding: t -*-
;;; Commentary:
;;; Code:

(defcustom milk-note
  "Emilks note-taking configuration."
  :type 'milk-note)

(defgroup milk-note nil
  "Emilks note configuration."
  :group 'milk-config
  :prefix "-")

(provide 'milk-note)
;;; milk-note.el ends here
