;;; milk-lib.el -- Misc. helper utilities -*- lexical-binding: t -*-
;;; Commentary:
;;; Code:

;;(require 'use-package)
;;(require 'bind-key)

(defun locate-expanded-emacs-file (relpath)
  "Locate RELPATH in user Emacs directory.
Returns expanded path. Creates file if missing."
  (expand-file-name (locate-user-emacs-file relpath)))

(defmacro use-feature (name &rest args)
  "Same as `use-package', but for pre-installed packages.
NAME and ARGS are as in `use-package'."
  (declare (indent defun))
  `(use-package ,name
     :straight nil
     ,@args))

(defmacro unbind-keys (args keymap)
  "Unbind the keys given in the list ARGS from KEYMAP."
  `(progn
     ,@(mapcar (lambda (arg) `(unbind-key ,arg ,keymap)) args)))


(defun flash-mode-line ()
  "Briefly invert  he face of the mode-line."
  (invert-face 'mode-line)
  (run-with-timer 0.1 nil #'invert-face 'mode-line))

(defun mode-line-toggle ()
  "Toggle the mode-line."
  (interactive)
  (defvar-local mode-line--toggle-format nil)
  (if mode-line-format
      (progn
        (setq mode-line--toggle-format mode-line-format)
        (setq mode-line-format nil))
    (setq mode-line-format mode-line--toggle-format)
    (setq mode-line--toggle-format nil))
  (redraw-frame))

(defun cursor-toggle ()
  "Toggle the cursor."
  (interactive)
  (setq cursor-type (not cursor-type)))

(provide 'milk-lib)
;;; milk-lib.el ends here
