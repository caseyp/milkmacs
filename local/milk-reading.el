;;; milk-reading.el -- Reading minor mode -*- lexical-binding: t -*-
;;; Commentary:
;;; Code:

(require 'milk-lib)

(defgroup reading-mode nil
  "Emilks reading mode configuration."
  :prefix "reading-")

(defcustom reading-font-scale 1.5
  "Amount to scale the font for reading."
  :type '(choice integer (const :tag "Default" 1.5)))

(defvar reading-mode-map
  (let ((map (make-sparse-keymap)))
      (define-key map [(control c) l] #'winner-undo)
      (define-key map [(control c) right] #'winner-redo))
    map)
  "Keymap for Reading mode.")

;; TODO local keymap toggling scaling / fill-column size / settings
;;  (face-remap-add-relative 'variable-pitch :height 1.5)
(define-minor-mode reading-mode
  "Toggle reading mode or off."
  :group 'reading
  :global nil
  :lighter nil
  (if reading-mode
      (progn
        (variable-pitch-mode +1)
        (when mode-line-format (mode-line-toggle))
        (when cursor-type (cursor-toggle))
        (defvar-local header-line-format-orig header-line-format)
        (setq-local header-line-format nil)
        (scroll-lock-mode +1))
    (variable-pitch-mode -1)
    (when (not mode-line-format) (mode-line-toggle))
    (when (not cursor-type) (cursor-toggle))
    (setq-local header-line-format header-line-format-orig)
    (scroll-lock-mode -1)))


(provide 'milk-reading)
;;; milk-reading.el ends here
