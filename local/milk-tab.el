;;; milk-tab.el -- Tab-bar configuration -*- lexical-binding: t -*-
;;; Commentary:
;;; Taken from consult wiki.
;;; TODO https://mihaiolteanu.me/emacs-workspace-management.html
;;; Code:

(require 'consult)
(require 'marginalia)

(defvar +consult--tab-index-current-tab-name nil
  "The name of the current tab. Needed for marginalia annotations when previewing tabs.
Because we are changing the current window configuration when previewing tabs, we are
also changing the name of the current tab unless it's not an explicit name. To prevent
this, we can store the name of the current tab before calling consult command and use
this saved name in marginalia annotations of the current tab.")

(defvar +consult--tab-index-current-tab-bufs nil
  "List of current tab buffer names. Needed for marginalia annotations when previewing tabs.
Because we are changing the current window configuration when previewing tabs, we need to
save the current list of buffers displayed in windows before calling consult command and
use this saved list in marginalia annotations of the current tab.")
(defun +marginalia-annotate-tab-index (cand)
  "Modified version of `marginalia-annotate-tab' suited for tab-index completion."
  (let* ((tab (nth (1- (string-to-number cand)) (tab-bar-tabs)))
         (current-p (memq 'current-tab tab))
         (ws (alist-get 'ws tab))
         (bufs (if current-p
                   +consult--tab-index-current-tab-bufs
                 (window-state-buffers ws))))
    ;; NOTE: When the buffer key is present in the window state
    ;; it is added in front of the window buffer list and gets duplicated.
    (unless current-p
      (when (cadr (assq 'buffer ws)) (pop bufs)))
    (marginalia--fields
     ;; Tab name
     ((if current-p
          +consult--tab-index-current-tab-name
        (alist-get 'name tab))
      :face (if current-p 'marginalia-on 'marginalia-key)
      :width 15
      :truncate 15)
     ;; Window count
     ((if (cdr bufs)
          (format "%d windows" (length bufs))
        "1 window ")
      :face 'marginalia-size
      :width 15)
     ;; List of buffers
     ((string-join bufs " \t ")
      :face 'marginalia-documentation))))

(add-to-list 'marginalia-annotator-registry '(tab-index +marginalia-annotate-tab-index))

(defun +consult--tab-index-preview ()
  "Preview function for tab-index."
  (let ((orig-wc (current-window-configuration)))
    (lambda (action cand)
      (if (eq action 'exit)
          (set-window-configuration orig-wc nil t)
        (when cand
          (set-window-configuration
           (alist-get 'wc (nth (1- (string-to-number cand))
                               (tab-bar-tabs))
                      ;; default to original wc if
                      ;; there is no tab wc (usually current tab)
                      orig-wc)
           nil t))))))

(defvar +consult--source-tab-index
  (list :name "Tab"
        :category 'tab-index
        :default t
        :narrow ?t
        :state #'+consult--tab-index-preview
        :items (lambda ()
                 (mapcar #'number-to-string
                         (number-sequence 1 (length (tab-bar-tabs))))))
  "Source of all tab indexes starting from 1.")

(defun +consult--tab-index (&optional prompt)
  "Prompt for tab selection and return selected candidate as number.
Replace prompt with PROMPT if specified."
  ;; Marginalia integration
  (let (;; Align annotations as close to index as possible
        (marginalia-align-offset -18)
        ;; Save curret tab name
        (+consult--tab-index-current-tab-name (alist-get 'name (tab-bar--current-tab)))
        ;; Save current window buffer list
        (+consult--tab-index-current-tab-bufs (mapcar #'buffer-name
                                                      (mapcar #'window-buffer
                                                              (window-list)))))
    (string-to-number (car (consult--multi '(+consult--source-tab-index)
                                           ;; disable sorting
                                           :sort nil
                                           :require-match t
                                           :prompt (or prompt "Select tab: "))))))

;;;###autoload
(defun +consult-tab ()
  "Select tab and switch to it."
  (interactive)
  (tab-bar-select-tab (+consult--tab-index)))

(defvar +consult--tab-index-commands '(+tab-bar-dwim
                                       +consult-tab
                                       +consult-tab-close*)
  "List of commands that will trigger `+consult--tab-index-preselect' and `+consult--tab-index-refresh'")

(defun +consult--tab-index-preselect ()
  "Preselect recent tab if `this-command' in `+consult--tab-index-commands'."
  (when (memq this-command +consult--tab-index-commands)
    (vertico--goto (or (tab-bar--tab-index-recent 1)
                       (tab-bar--current-tab-index)))))

(add-hook 'minibuffer-setup-hook #'+consult--tab-index-preselect)

(defun +consult--tab-index-refresh ()
  "Run `consult--vertico-refresh' if `this-command' in `+consult--tab-index-commands'."
  (when (memq this-command +consult--tab-index-commands)
    (consult--vertico-refresh)))

(advice-add #'vertico--setup :after #'+consult--tab-index-refresh)

;;;###autoload
(defun +tab-bar-dwim (&optional arg)
  "Do-What-I-Mean function for tabs.
If optional prefix argument is specified, then switch to `ARG'th tab.

If no other tab exists, create one and switch to it.

If there is one other tab (two in total), switch to it.

If there are more than two tabs, select tab with `+consult-tab'."
  (interactive "P")
  (if arg
      (tab-bar-select-tab arg)
    (pcase (length (tab-bar-tabs))
      (1 (tab-bar-new-tab))
      (2 (tab-bar-switch-to-next-tab))
      (_ (+consult-tab)))))

;;;###autoload
(defun +consult-tab-close* ()
  "Close multiple tabs."
  (interactive)
  (let (index)
    (while (setq index (+consult--tab-index "Close tab: "))
      (tab-bar-close-tab index))))

(provide 'milk-tab)
;;; milk-tab.el ends here
