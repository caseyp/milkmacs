;;; milk-bib.el -- Bibliographic management -*- lexical-binding: t -*-
;;; Commentary:
;;; Code:

(require 'url)
(require 'url-http)

(require 'milk-config)
(require 'use-package)

(use-package citeproc)

(use-package parsebib
  :straight (parsebib :type git :host github :repo "joostkremers/parsebib"))

(use-feature bibtex
  :custom
  (bibtex-autokey-titleword-case-convert 'upcase)
  (bibtex-autokey-name-year-separator    "")
  (bibtex-autokey-year-title-separator   "")
  (bibtex-autokey-year-length            4)
  (bibtex-autokey-titlewords             3)
  (bibtex-autokey-titlewords-stretch     0)
  (bibtex-autokey-titleword-length       -1)
  (bibtex-autokey-titleword-separator    ""))

(use-package citar
  :bind (:map org-mode-map
              ("C-c b b" . citar-insert-citation)
              ("C-c b r" . citar-insert-reference)
              ("C-c b o" . citar-open-notes))
  :hook ((LaTeX-mode org-mode) . citar-capf-setup)
  :custom
  (citar-at-point-function 'embark-act)
  (citar-library-paths     `(,eml-bib-pdf-storage-directory))
  (citar-bibliography      `(,eml-bib-main-file)))

(use-feature citar-org
  :after (citar oc)
  :custom
  (org-cite-insert-processor   'citar)
  (org-cite-follow-processor   'citar)
  (org-cite-activatpppe-processor 'citar))


(use-package biblio
  :custom
  (biblio-bibtex-use-autokey t)
  ;; Set it, but download through ebib.
  (biblio-download-directory eml-bib-pdf-storage-directory)
  (biblio-bibtex-file        eml-bib-main-file))

(use-package ebib
  :bind (:map ebib-index-mode-map
              ("D" . ebib-download-url))
  :custom
  (ebib-bibtex-dialect    'biblatex)
  (ebib-file-associations nil)
  (ebib-preload-bib-files `(,eml-bib-main-file))
  (ebib-file-search-dirs  `(,eml-bib-pdf-storage-directory))
  (ebib-notes-directory   eml-bib-pdf-storage-directory)
  (ebib-notes-locations   eml-bib-pdf-storage-directory)
  (ebib-reading-list-file (concat eml-bib-pdf-storage-directory "reading-list.org"))

  :config

  (defun ebib-transform-doi-url (url)
    "Transform a doi URL to the scihub URL of its corresponding pdf file."
    (let* ((url-request-method "POST")
           (url-request-data (url-build-query-string `(("request" ,url))))
           (url-mime-language-string "en-US,en;q=0.5")
           (url-request-extra-headers '(("Content-Type" . "application/x-www-form-urlencoded")))
           (res-buffer (url-retrieve-synchronously eml-bib-scihub-homepage nil nil 2)))
      (when res-buffer
        (let* ((dom (with-current-buffer res-buffer
                      (goto-char (1+ url-http-end-of-headers))
                      (libxml-parse-html-region (point) (point-max))))
               (save-link (dom-attr
                           (dom-search dom
                                       (lambda (d) (and (stringp (caddr d))
                                                        (string-suffix-p " save" (caddr d))))) 'onclick)))
          (if (not save-link) (signal 'scihub-not-found (list url))
            (let ((url (string-remove-suffix "'" (string-remove-prefix "location.href='" save-link))))
              (string-remove-suffix "?download=true"
                                    (if (string-match-p url-nonrelative-link url)
                                        url
                                      (url-expand-file-name url eml-bib-scihub-homepage)))))))))
  (add-to-list 'ebib-url-download-transformations '("https?://doi.org/"    . ebib-transform-doi-url))
  (add-to-list 'ebib-url-download-transformations '("https?://dx.doi.org/" . ebib-transform-doi-url)))

(use-feature ebib-biblio
  :after (ebib biblio)
  :bind (:map ebib-index-mode-map
              ("B" . ebib-biblio-import-doi)
         :map biblio-selection-mode-map
              ("e" . ebib-biblio-selection-import)))

(provide 'milk-bib)
;;; milk-bib.el ends here
