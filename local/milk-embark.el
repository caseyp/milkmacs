;;; milk-embark.el -- Embark configuration -*- lexical-binding: t -*-
;;; Commentary:
;;; Code:

(require 'use-package)
(require 'marginalia)
(require 'which-key)

(use-package embark
  :demand t
  :bind (("C-." . embark-act)
         ("M-." . embark-dwim)
         ([remap describe-bindings]  . embark-bindings)
         :map embark-general-map
         ("I" . embark-inject)
         :map embark-variable-map
         ("e" . embark-act-with-eval)
         :map embark-expression-map
         ("e" . embark-act-with-eval)
         :map embark-defun-map
         ("e" . embark-act-with-eval)
         )

  :custom
  (embark-indicators '(embark-which-key-indicator
                       embark-highlight-indicator
                       embark-isearch-highlight-indicator))
  ;; (prefix-help-command #'embark-prefix-help-command)

  :config

  ;; Use which-key based indicator
  (defun embark-which-key-indicator ()
    "An embark indicator that displays keymaps using which-key.
The which-key help message will show the type and value of the
current target followed by an ellipsis if there are further
targets."
    (lambda (&optional keymap targets prefix)
      (if (null keymap)
          (which-key--hide-popup-ignore-command)
        (which-key--show-keymap
         (if (eq (plist-get (car targets) :type) 'embark-become)
             "Become"
           (format "Act on %s '%s'%s"
                   (plist-get (car targets) :type)
                   (embark--truncate-target (plist-get (car targets) :target))
                   (if (cdr targets) "…" "")))
         (if prefix
             (pcase (lookup-key keymap prefix 'accept-default)
               ((and (pred keymapp) km) km)
               (_ (key-binding prefix 'accept-default)))
           keymap)
         nil nil t (lambda (binding) (not (string-suffix-p "-argument" (cdr binding))))))))

  (advice-add #'embark-completing-read-prompter
              :around
              #'embark-hide-which-key-indicator)
  (defun embark-hide-which-key-indicator (fn &rest args)
    "Hide the which-key indicator immediately when using the completing-read prompter."
    (which-key--hide-popup-ignore-command)
    (let ((embark-indicators
           (remq #'embark-which-key-indicator embark-indicators)))
      (apply fn args)))

  ;; Straight package management
  (defvar-keymap embark-straight-map
    :parent embark-general-map
    "u" #'straight-visit-package-website
    "r" #'straight-get-recipe
    "i" #'straight-use-package
    "c" #'straight-check-package
    "F" #'straight-pull-package
    "f" #'straight-fetch-package
    "p" #'straight-push-package
    "n" #'straight-normalize-package
    "m" #'straight-merge-package)
  (add-to-list 'marginalia-prompt-categories '("recipe\\|package" . straight))
  (add-to-list 'embark-keymap-alist '(straight . embark-straight-map))

  ;; Elisp eval action
  (defun embark-act-with-eval (expression)
    "Evaluate EXPRESSION and call `embark-act' on the result."
    (interactive "sExpression: ")
    (with-temp-buffer
      (insert (eval (read expression)))
      (embark-act)))

  ;; Insert into active minibuffer
  (defun embark-inject (str)
    (interactive "sString: ")
    (let ((win (active-minibuffer-window)))
      (unless win
        (user-error "No active minibuffer"))
      (select-window win)
      (delete-minibuffer-contents)
      (insert str)))
)

(use-package embark-consult
  :demand t
  :hook (embark-collect-mode . consult-preview-at-point-mode))




(provide 'milk-embark)
;;; milk-embark.el ends here
