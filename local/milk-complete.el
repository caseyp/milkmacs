;;; milk-complete.el -- Adapters for completion -*- lexical-binding: t -*-
;;; Commentary:
;;; Code:

(require 'milk-lib)

(use-feature emacs
  :custom
  (read-file-name-completion-ignore-case t)
  (read-buffer-completion-ignore-case    t)
  (completion-ignore-case                nil))

;; Completion framework.
(use-package vertico
  :straight (vertico :files (:defaults "extensions/*"))
  :bind (:map vertico-map
              ("RET"   . vertico-directory-enter)
              ("M-DEL" . vertico-directory-delete-word))
  :custom
  (vertico-preselect     'first)
  (vertico-scroll-margin 0)
  (vertico-cycle         t)
  (vertico-resize        t)
  :config
  (vertico-mode +1))

(use-feature vertico-buffer)
(use-feature vertico-directory)
(use-feature vertico-flat)
(use-feature vertico-indexed)
(use-feature vertico-quick
  :bind (:map vertico-map ("C-;" . vertico-quick-insert)))

;; Redo a previous invocation of completion.
(use-feature vertico-repeat
  :bind (("M-R" . vertico-repeat-select))
  :hook (minibuffer-setup . vertico-repeat-save))

;; Allow different appearances to completion on a per-command basis.
(use-feature vertico-multiform
  :custom
  (vertico-multiform-commands   '((embark-prefix-help-command grid)))
  (vertico-multiform-categories '((file   grid)))
  :config
  (vertico-multiform-mode +1))

;; In-line completion.
(use-package corfu
  :straight (corfu :files (:defaults "extensions/*"))
  :demand t
  :bind (:map corfu-map
              ([tab]     . corfu-next)
              ([backtab] . corfu-previous)
              ("M-m"     . corfu-move-to-minibuffer))
  :custom
  (corfu-auto       t)
  (corfu-cycle      t)
  (corfu-preselect  'first)
  (corfu-auto-delay 0.2)
  (corfu-auto-prefix 2)
  :config
  (global-corfu-mode)
  (defun corfu-move-to-minibuffer ()
    (interactive)
    (when completion-in-region--data
      (let ((completion-extra-properties corfu--extra)
            completion-cycle-threshold completion-cycling)
	(apply #'consult-completion-in-region completion-in-region--data))))
  (add-to-list 'corfu-continue-commands #'corfu-move-to-minibuffer))

(use-feature corfu-popupinfo
  :after (corfu)
  :demand t
  :custom
  (corfu-popupinfo-delay '(0.5 . 0.1))
  :config
  (corfu-popupinfo-mode))

;; Matching code.
(use-package orderless
  :demand t
  :custom
  (orderless-matching-styles     '(orderless-initialism orderless-literal orderless-regexp))
  (orderless-component-separator #'orderless-escapable-split-on-space)
  (orderless-style-dispatchers   '(orderless-file-ext-dispatch orderless-affix-dispatch))
  :config
  (defun orderless-file-ext-dispatch (word _index _total)
    (when (and (or minibuffer-completing-file-name (derived-mode-p 'eshell-mode))
               (string-match-p "\\`\\.." word))
      `(orderless-regexp . ,(concat "\\." (substring word 1) "$")))))

;; Matching code, with sort based on history.
(use-package prescient
  :custom
  (prescient-use-case-folding        'smart)
  (prescient-filter-method           '(literal))
  (prescient-use-char-folding        t)
  (prescient-sort-full-matches-first t)
  (prescient-sort-length-enable      t)
  (prescient-history-length          500)
  :config
  (prescient-persist-mode +1))

(use-package vertico-prescient
  :after (vertico prescient)
  :custom
  (vertico-prescient-enable-filtering nil))

;; XXX Copied from vertico wiki
(defun vertico-bottom--display-candidates (lines)
  "Display LINES in bottom."
  (move-overlay vertico--candidates-ov (point-min) (point-min))
  (unless (eq vertico-resize t)
    (setq lines (nconc (make-list (max 0 (- vertico-count (length lines))) "\n") lines)))
  (let ((string (apply #'concat lines)))
    (add-face-text-property 0 (length string) 'default 'append string)
    (overlay-put vertico--candidates-ov 'before-string string)
    (overlay-put vertico--candidates-ov 'after-string nil))
  (vertico--resize-window (length lines)))

(advice-add #'vertico--display-candidates :override #'vertico-bottom--display-candidates)

(defun minibuffer-send-down ()
  "Send down to minibuffer, even if it is not selected."
  (interactive)
  (when-let (minibuffer (active-minibuffer-window))
    (with-selected-window minibuffer
      (execute-kbd-macro [down]))))

(defun minibuffer-send-up ()
  "Send up to minibuffer, even if it is not selected."
  (interactive)
  (when-let (minibuffer (active-minibuffer-window))
    (with-selected-window minibuffer
      (execute-kbd-macro [up]))))

(defun other-window-minibuffer ()
  "Go between minibuffer and windows."
  (interactive)
  (defvar switch-to-minibuffer-prev-window nil)
  (if (window-minibuffer-p (selected-window))
      (select-window (or switch-to-minibuffer-prev-window
                         (minibuffer-selected-window)))
    (when-let (minibuffer (active-minibuffer-window))
      (setq switch-to-minibuffer-prev-window (selected-window))
      (select-window minibuffer))))

(bind-keys ("C-\\" . minibuffer-send-down)
           ("C-/"  . minibuffer-send-up)
           ("C-_"  . other-window-minibuffer))

(use-feature minibuffer
  :custom
  (read-extended-command-predicate   #'command-completion-default-include-p)
  ;; Choose orderless, prescient, etc.
  (completion-styles             '(orderless basic))
  (completion-category-defaults  nil)
  (completion-category-overrides '((file (styles partial-completion)))))

(provide 'milk-complete)
;;; milk-complete.el ends here
