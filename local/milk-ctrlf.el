;;; milk-ctrlf.el -- Adapters for ctrlf -*- lexical-binding: t -*-
;;; Commentary:
;;; Code:

(require 'avy)

(defun embark-ctrlf-forward (str)
  "Prompt for string in the minibuffer and start ctrlf-forward."
  (interactive "sSearch: ")
  (ctrlf-forward ctrlf-default-search-style nil str nil t))

(defun embark-ctrlf-backward (str)
  "Prompt for string in the minibuffer and start ctrlf-forward."
  (interactive "sSearch: ")
  (setq ctrlf--backward-p t)
  (ctrlf--start str))

(defun consult-ctrlf-history ()
  "Read a search string with completion from the ctrlf history.

This replaces the current search string if ctrlf is active, and
starts a new ctrlf session otherwise."
  (interactive)
  (ctrlf-forward ctrlf-default-search-style nil
                 (consult--read
                  ctrlf-search-history
                  :prompt "CTRLF search: "
                  :category 'consult-ctrlf-history
                  :history t
                  :sort nil
                  :lookup
                  (lambda (selected candidates &rest _)
                    (if-let (found (member selected candidates))
                        (substring (car found) 0 -1)
                      selected)))))

(defun avy-ctrlf ()
  "Jump to one of the current ctrlf candidates."
  (interactive)
  (avy-with avy-ctrlf
    (let ((avy-background nil)
          (avy-case-fold-search case-fold-search))
      (prog1
          (avy-process
           (avy--regex-candidates (if (eq ctrlf--style 'regexp)
                                      ctrlf--last-input
                                    (regexp-quote ctrlf--last-input))))
        (exit-minibuffer)))))

(provide 'milk-ctrlf)
;;; milk-ctrlf.el ends here

