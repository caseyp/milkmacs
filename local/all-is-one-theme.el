;;; all-is-one-theme.el  --- Summary -*- lexical-binding: t; -*-

;;; Commentary:
;;; An elegant, ocean theme for Emacs.
;;; Based on https://www.pixiv.net/en/artworks/74932889
;;;
;;; Code:

(deftheme all-is-one "Nel silenzio tragiche realta.")

(let ((class '((class color) (min-colors 89)))
      (black-1  "#000506") (black-2  "#121928") (black-3 "#2f223d") (black-4 "#2f2241") (black-5 "#182645")
      (white-1  "#fff8ec") (white-2  "#f5f6f7") (white-3 "#e2e6ea")
      (red-1    "#eb9d71") (red-2    "#ce8f66") (red-3   "#db7355") (red-4   "#9b2d40")
      (pink-1   "#febb8d") (pink-2   "#e28d8a") (pink-3  "#b9798b")
      (tan-1    "#e4d8ba") (tan-2    "#ead6b6") (tan-3   "#ffd5a8") (tan-4   "#debd94") (tan-5  "#c7a682")
      (sky-1    "#d6eefc") (sky-2    "#86f4f9") (sky-3   "#78d8f3") (sky-4   "#7cb9f4")
      (sea-1    "#2d99ba") (sea-2    "#2180a1") (sea-3   "#20507c") (sea-4   "#282d70")
      (cyan-1   "#adc6b9") (cyan-2   "#69bdb6") (cyan-3  "#4da79e")
      (green-1  "#7ceeae") (green-2  "#03e293")
      (yellow-1 "#fad84c") (yellow-2 "#b79241")
      (grey-1   "#6e5461") (grey-2   "#634e61") (grey-3  "#614d60") (grey-4  "#604950") (grey-5  "#474246")
      (btan-1   "#937c60") (btan-2   "#8c765c") (btan-3  "#887359") (btan-4  "#a79083")
      (gtan-1   "#ccccb8") (gtan-2   "#cacbb6") (gtan-3  "#8b9d90") (gtan-4  "#9c8986"))
  (defconst dark-bg     black-2)
  (defconst dim-bg      black-5)
  (defconst light-bg    sea-4)

  (defconst hl-row      black-4)

  (defconst white-fg    white-3)
  (defconst bright-fg   white-1)
  (defconst dim-fg      sea-2)
  (defconst light-fg    sky-2)
  (defconst dark-fg     sea-4)

  (defconst dark-hl     grey-5)
  (defconst dim-hl      gtan-3)
  (defconst light-hl    sky-1)

  (defconst bright-ok   green-2)
  (defconst dim-ok      green-1)
  (defconst bright-warn yellow-1)
  (defconst dim-warn    yellow-2)
  (defconst bright-err  red-4)
  (defconst dim-err     red-3)

  (defconst dim-border  sea-3)

  (custom-theme-set-faces
   'all-is-one
   `(default ((((class color) (min-colors 4096))
               (:foreground ,white-fg :background ,dark-bg))
              (((class color) (min-colors 256))
               (:foreground ,white-fg :background ,dark-bg))
              (,class
               (:foreground ,white-fg :background ,dark-bg))))

   `(cursor
     ((,class (:background ,bright-fg))))

   `(region
     ((,class (:background ,dim-hl))))

   `(trailing-whitespace
     ((,class (:background ,bright-err))))

   `(show-paren-match
     ((,class (:background ,cyan-3))))
   `(show-paren-mismatch
     ((,class (:background ,sky-2))))

   `(hl-line
     ((,class (:background ,hl-row))))

   `(line-number
     ((,class (:foreground ,dim-fg))))
   `(line-number-current-line
     ((,class (:foreground ,bright-warn :background ,dark-bg))))

   `(mode-line
     ((,class (:foreground ,light-fg :background ,light-bg))))
   `(mode-line-inactive
     ((,class (:foreground ,dim-fg   :background ,dim-bg))))

   `(mode-line-buffer-id
     ((,class (:foreground ,bright-fg :weight bold))))
   `(mode-line-buffer-id-inactive
     ((,class (:foreground ,dim-fg    :weight bold))))

   `(vertical-border
     ((,class (:foreground ,dim-border))))

   `(minibuffer-prompt
     ((,class (:foreground nil :background nil))))

   `(success
     ((,class (:foreground ,bright-ok))))
   `(warning
     ((,class (:foreground ,bright-warn))))
   `(error
     ((,class (:foreground ,bright-err))))

   `(font-lock-comment-face
     ((,class (:foreground ,tan-5 :slant italic))))
   `(font-lock-comment-delimiter-face
     ((,class (:foreground ,grey-3))))
   `(font-lock-doc-face
     ((,class (:background unspecified :foreground ,gtan-1))))
   `(font-lock-doc-string-face
     ((,class (:background unspecified :foreground ,gtan-1))))
   `(font-lock-negation-char-face
     ((,class (:background unspecified :foreground ,dim-warn))))
   `(font-lock-builtin-face
     ((,class (:foreground ,pink-3))))
   `(font-lock-constant-face
     ((,class (:foreground ,sky-4))))
   `(font-lock-variable-name-face
     ((,class (:foreground ,tan-1))))
   `(font-lock-function-name-face
     ((,class (:foreground ,yellow-1))))
   `(font-lock-type-face
     ((,class (:foreground ,pink-1))))
   `(font-lock-keyword-face
     ((,class (:foreground ,green-1))))
   `(font-lock-string-face
     ((,class (:foreground ,sky-3))))
   `(font-lock-preprocessor-face
     ((,class (:foreground ,dim-fg))))
   `(font-lock-warning-face
     ((,class (:foreground ,dim-warn :weight bold))))

   `(link
     ((,class (:underline t :foreground ,sky-3))))
   `(link-visited
     ((,class (:underline t :foreground ,sky-4))))

   `(highlight
     ((,class (:background ,sea-3 :foreground nil :underline t :weight bold))))

   `(vterm-color-blue
     ((,class (:foreground ,sea-1 :background ,sky-1))))

   `(goggles-added
     ((,class (:background ,bright-warn))))
   `(goggles-changed
     ((,class (:background ,dim-bg))))
   `(goggles-removed
     ((,class (:background ,dim-err))))

   `(avy-background-face
     ((,class (:foreground ,dark-fg))))
   `(avy-lead-face
     ((,class (:background ,light-bg))))
   `(avy-lead-face-0
     ((,class (:background ,light-bg))))
   `(avy-lead-face-1
     ((,class (:background ,light-bg))))
   `(avy-lead-face-2
     ((,class (:background ,light-bg))))

   `(vertico-current
     ((,class (:inherit highlight))))

   `(marginalia-documentation
     ((,class (:foreground ,gtan-1))))

   ;; TODO tempel
   ;; TODO jinx
   ;; TODO magit
   ;; TODO ...

   `(agda2-highlight-keyword-face
     ((,class (:inherit  font-lock-keyword-face))))
   `(agda2-highlight-comment-face
     ((,class (:inherit  font-lock-comment-face))))
   `(agda2-highlight-string-face
     ((,class (:inherit  font-lock-string-face))))
   `(agda2-highlight-function-face
     ((,class (:inherit  font-lock-function-name-face))))
   `(agda2-highlight-primitive-type-face
     ((,class (:inherit  font-lock-type-face))))
   `(agda2-highlight-primitive-face
     ((,class (:inherit  font-lock-constant-face))))
   `(agda2-highlight-datatype-face
     ((,class (:inherit  font-lock-type-face))))
   `(agda2-highlight-pragma-face
     ((,class (:inherit  font-lock-preprocessor-face))))
   `(agda2-highlight-errorwarning-face
     ((,class (:inherit  font-lock-warning-face))))
   `(agda2-highlight-inductive-constructor-face
     ((,class (:inherit  font-lock-constant-face))))
   `(agda2-highlight-field-face
     ((,class (:inherit  font-lock-function-name-face))))
   )

  (custom-theme-set-variables
   'all-is-one
   `(ansi-color-names-vector
     [,black-3 ,red-4 ,green-1 ,yellow-1 ,sea-3 ,sky-1 ,cyan-1 ,white-3])))

(provide-theme 'all-is-one)

;;; all-is-one-theme.el ends here
