;;; reading.el -- Reading minor mode -*- lexical-binding: t -*-
;;; Commentary:
;;; Code:

(require 'milk-lib)

(defgroup reading-mode nil
  "Emilks reading mode configuration."
  :prefix "reading-")

(defcustom reading-font-scale 1.5
  "Amount to scale the font for reading."
  :type '(choice integer (const :tag "Default" 1.5)))

(defvar reading--relative-cookie nil)

(defun reading-large-print-toggle ()
  "Toggle large print based on `reading-font-scale`."
  (interactive)
  (if (not reading--relative-cookie)
      (setq reading--relative-cookie
            (face-remap-add-relative 'variable-pitch :height reading-font-scale))
    (face-remap-remove-relative reading--relative-cookie)
    (setq reading--relative-cookie nil)))

(defun reading-set-fill-column (len)
  "Set buffer local `visual-fill-column-width' to LEN."
  (interactive "nColumn length: ")
  (setq-local visual-fill-column-width len))

;;;###autoload
(define-minor-mode reading-mode
  "Toggle reading mode or off."
  :group 'reading-mode
  :global nil
  :lighter t
  (if reading-mode
      (progn
        (visual-line-mode +1)
        (visual-fill-column-mode +1)
        (variable-pitch-mode +1)
        (when mode-line-format (mode-line-toggle))
        (when cursor-type (cursor-toggle))
        (defvar-local reading--header-line-format-orig header-line-format)
        (setq-local header-line-format nil)
        (scroll-lock-mode +1))
    (visual-line-mode -1)
    (visual-fill-column-mode -1)
    (variable-pitch-mode -1)
    (when (not mode-line-format) (mode-line-toggle))
    (when (not cursor-type) (cursor-toggle))
    (setq-local header-line-format reading--header-line-format-orig)
    (scroll-lock-mode -1)))

(provide 'reading)
;;; reading.el ends here
